#!/usr/bin/env bash

DOCKER_CONTAINERS=( calibre-web-app jellyfin-app nextcloud-app plex-app syncthing-app traefik-app unifi-app )

info() { printf "%s %s\n" "$(date)" "$*" 2>>$BORG_LOG 1>&2; }
warn() { info "WARN: $*"; }
error() { info "ERROR: $*"; return -1; }

for CONTAINER in ${DOCKER_CONTAINERS[@]}
do
    INSPECT="$(docker inspect $CONTAINER)"
    HEALTH="$(echo $INSPECT | jq -r '.[].State.Health.Status')"
    RUNNING="$(echo $INSPECT | jq -r '.[].State.Running')"
    if [[ ("$HEALTH" != "null" && "$HEALTH" != "healthy") || "$RUNNING" != "true" ]]; then
        error "$CONTAINER is not healthy or running."
    fi
done
