#!/usr/bin/env bash

# Initialize global environment variables.
export DATE=$(date +%Y%m%d)
export BORG_REPOS_DIR=$(jq -r '.repos_dir' ~/.borgbackup/settings.json)
export BORG_PASSPHRASE=$(jq -r '.passphrase' ~/.borgbackup/settings.json)
export BORG_LOG="~/.borgbackup/logs/$DATE.log"
export BUCKET_PREFIX=$(jq -r '.bucket_prefix' ~/.borgbackup/settings.json)
LV_BLACKLIST=(lv-ytsubs lv-plex lv-vol)

# Helpers and error handlers
info() { printf "%s %s\n" "$(date)" "$*" 2>>$BORG_LOG 1>&2; }
warn() { info "WARN: $*"; }
error() { info "ERROR: $*"; return -1; }
trap 'echo $(date) Backup interrupted >&2; exit 2' INT TERM

# Initialize the borg backup log
initialize_log() {
    # Create the log directory if it doesn't exist.
    BORG_LOG_DIR=$(dirname "$BORG_LOG")
    if [ ! -d "$BORG_LOG_DIR" ]; then
      mkdir -p "$(dirname \"$BORG_LOG\")"
    fi

    # Create the log file if it doesn't exist.
    if [ ! -f "$BORG_LOG" ]; then
      touch "$BORG_LOG"
    fi
}

# Calculates the snapshot size for the logical volume. 5% of lv size.
calculate_snapshot_size() {
    LV="$1"
    LV_SIZE=$(lvs --no-headings -o lv_size -S "lvname=$LV")

    # Get size of the logical volume.
    LV_SIZE_VALUE=
    LV_SIZE_UNITS=
    if [[ $LV_SIZE =~ ([0-9]+\.[0-9]+)(t|g|m|k) ]]; then
        LV_SIZE_VALUE=${BASH_REMATCH[1]}
        LV_SIZE_UNITS=${BASH_REMATCH[2]}
        info "Logical Volume Size: $LV_SIZE_VALUE$LV_SIZE_UNITS"

        # Calculate snapshot size (5% of logical volume)
        SNAPSHOT_SIZE_VALUE=
        SNAPSHOT_SIZE_UNITS=
        if (( $(echo "$LV_SIZE_VALUE < 20" | bc) )); then
            # If logical volume is less than 20 units, reduce units and
            # increase size to match. Perform calculation to get size.
            SNAPSHOT_SIZE_VALUE=$(echo "($LV_SIZE_VALUE * 1000) / 20" | bc)
            case "$LV_SIZE_UNITS" in
                "t")
                    SNAPSHOT_SIZE_UNITS="g"
                    ;;
                "g")
                    SNAPSHOT_SIZE_UNITS="m"
                    ;;
                "m")
                    SNAPSHOT_SIZE_UNITS="k"
                    ;;
            esac
        else
            SNAPSHOT_SIZE_VALUE=$(echo "$LV_SIZE_VALUE / 20" | bc)
            SNAPSHOT_SIZE_UNITS=$LV_SIZE_UNITS
        fi
    fi

    info "Snapshot Size: $SNAPSHOT_SIZE_VALUE$SNAPSHOT_SIZE_UNITS"
    echo "$SNAPSHOT_SIZE_VALUE$SNAPSHOT_SIZE_UNITS"
}

create_backup() {
    LV="$1"
    SNAPSHOT_NAME="$2"

    borg create --compression=lz4 "$BORG_REPOS_DIR/$LV::$SNAPSHOT_NAME" /mnt/snap
}

prune_backups() {
    LV="$1"
    DEFAULT_KEEP_YEARLY=1
    DEFAULT_KEEP_MONTHLY=12
    DEFAULT_KEEP_WEEKLY=4
    DEFAULT_KEEP_DAILY=7

    # Query settings file for prune settings for logical volume
    if [ -f ~/.borgbackup/settings.json ]; then
        KEEP_YEARLY=$(jq -r ".prune_settings[] | select(.logical_volume == \"$LV\") | .keep_yearly" ~/.borgbackup/settings.json)
        KEEP_MONTHLY=$(jq -r ".prune_settings[] | select(.logical_volume == \"$LV\") | .keep_monthly" ~/.borgbackup/settings.json)
        KEEP_WEEKLY=$(jq -r ".prune_settings[] | select(.logical_volume == \"$LV\") | .keep_weekly" ~/.borgbackup/settings.json)
        KEEP_DAILY=$(jq -r ".prune_settings[] | select(.logical_volume == \"$LV\") | .keep_daily" ~/.borgbackup/settings.json)
    fi

    # Use default values if prune settings weren't found.
    KEEP_YEARLY=${KEEP_YEARLY:-$DEFAULT_KEEP_YEARLY}
    KEEP_MONTHLY=${KEEP_MONTHLY:-$DEFAULT_KEEP_MONTHLY}
    KEEP_WEEKLY=${KEEP_WEEKLY:-$DEFAULT_KEEP_WEEKLY}
    KEEP_DAILY=${KEEP_DAILY:-$DEFAULT_KEEP_DAILY}

    info "Backup Retention Settings\n=========================="
    info "Yearly Backups: $KEEP_YEARLY"
    info "Monthly Backups: $KEEP_MONTHLY"
    info "Weekly Backups: $KEEP_WEEKLY"
    info "Daily Backups: $KEEP_DAILY"

    # Prune backups for logical volume.
    borg prune --list --show-rc --keep-yearly=$KEEP_YEARLY --keep-monthly=$KEEP_MONTHLY --keep-weekly=$KEEP_WEEKLY --keep-daily=$KEEP_DAILY "$BORG_REPOS_DIR/$LV"
}

backup_to_b2() {
    LV="$1"
    BORG_REPO="$BORG_REPOS_DIR/$LV"
    BUCKET="$BUCKET_PREFIX-$LV"
    rclone sync "$BORG_REPO" backblaze:$BUCKET
}

if [ -z "$BORG_REPOS_DIR" ] || [ -z "$BORG_PASSPHRASE" ] || [ -z "$BUCKET_PREFIX" ]; then
    error "Unable to read settings file"
fi

# Initialize the log file.
initialize_log

# Write session header to the log file.
printf "\n\n===== %s =====\n" "$(date)" >> "$BORG_LOG"

LOGVOLS=$(lvs --no-headings -o lv_name -S "vgname=vg1")
for LV in ${LOGVOLS[@]}
do
    SNAPSHOT_NAME="$LV-$DATE"
    info "Logical Volume: $LV"
    info "Snapshot: $SNAPSHOT_NAME"

    # Calculate snapshot size and create the snapshot.
    # Snapshots will have a size equal to 5% of the size of the logical volume.
    SNAPSHOT_SIZE=$(calculate_snapshot_size "$LV")
    if lvcreate -q -s -L $SNAPSHOT_SIZE -n "$SNAPSHOT_NAME" "/dev/vg1/$LV"; then
        info "Created the $SNAPSHOT_NAME snapshot with a size of $SNAPSHOT_SIZE."
    else
        error "Failed to create the $SNAPSHOT_NAME snapshot."
    fi

    # Create the /mnt/snap directory if it doesn't exist.
    if [ ! -d "/mnt/snap" ]; then
        if mkdir -p "/mnt/snap"; then
            info "Created directory for mounting snapshots."
        else
            error "Failed to create directory for mounting snapshots."
        fi
    fi

    # Mount the snapshot to the /mnt/snap directory.
    if mount /dev/vg1/$SNAPSHOT_NAME /mnt/snap; then
        info "Mounted the $SNAPSHOT_NAME snapshot."
    else
        error "Failed to mount the $SNAPSHOT_NAME snapshot."
    fi

    # Create backup for logical volume
    if create_backup $LV $SNAPSHOT_NAME; then
        info "Created a backup of the $LV logical volume using data from the $SNAPSHOT_NAME snapshot."
    else
        error "Failed to create a backup for the $LV logical volume."
    fi

    # Prune backups for logical volume
    if prune_backups $LV; then
        info "Pruned backups of the $LV logical volume."
    else
        error "Failed to pruned the backups of the $LV logical volume."
    fi

    if [[ ! "${LV_BLACKLIST[*]}" =~ "${LV}" ]]; then
        # rclone to Backblaze
        if backup_to_b2 $LV; then
            info "Synchronized $BORG_REPOS_DIR/$LV to Backblaze B2."
        else
            error "Failed to synchronize $BORG_REPOS_DIR/$LV to Backblaze B2."
        fi
    else
        info "Skipping the blacklisted $LV logical volume."
    fi

    # Delete LVM snapshot
    if umount /mnt/snap; then
        info "Unmounted $SNAPSHOT_NAME from /mnt/snap."
        if lvremove -q --force "/dev/vg1/$SNAPSHOT_NAME"; then
            info "Removed the $SNAPSHOT_NAME snapshot."
        else
            warn "Failed to remove the $SNAPSHOT_NAME snapshot."
        fi
    else
        error "Failed to unmount the $SNAPSHOT_NAME snapshot from /mnt/snap."
    fi
done
