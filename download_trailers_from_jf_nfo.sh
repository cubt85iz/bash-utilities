#!/usr/bin/env bash

BASEDIR="/tank/philip/Media/Video/Movies/Features"
while read nfo; do
  FILENAME=$(basename "$nfo")
  MOVIENAME="${FILENAME%%.nfo}"
  TRAILERS=$(sed -n 's|<trailer>\(.*\)</trailer>|\1|p' "$nfo")

  # If nfo file contains trailer tags.
  if [ ! -z "$TRAILERS" ]; then
    # Change to movie directory.
    pushd "$BASEDIR/$MOVIENAME" > /dev/null

    # Determine if trailer already exists.
    if [ -f "$MOVIENAME-trailer.mp4" ]; then
      HAS_TRAILER=0
    else
      HAS_TRAILER=1
    fi

    # Iterate over trailers listed in nfo and download if trailer doesn't exist already.
    while read trailer; do
      if [ "$HAS_TRAILER" != "0" ]; then
        echo
        echo "=========================="
        echo "Downloading trailer for $MOVIENAME"
        echo "=========================="
        TRAILER=$(echo "$trailer" | sed 's/plugin:\/\/plugin.video.youtube\/?action=play_video&amp;videoid=/https:\/\/youtu.be\//g')
        /home/philip/.local/bin/yt-dlp -f 'bestvideo+bestaudio' --merge-output-format "mp4" -o "$MOVIENAME-trailer.%(ext)s" "$TRAILER"
        HAS_TRAILER=$?
 
        # Clean up files if failure occurs.
        if [ "$HAS_TRAILER" != "0" ]; then
          find -iname "*.webm" -exec rm {} \;
        fi
      fi
    done <<< "$TRAILERS"

    popd > /dev/null
  fi
done <<< "$(find $BASEDIR -iname '*.nfo' | sort)"
