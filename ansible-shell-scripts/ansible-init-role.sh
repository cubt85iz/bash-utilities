#!/usr/bin/env bash

display_usage() {
    echo "ERROR: Role path missing."
}

ROLE="$1"

# Create playbook directory if it doesn't exist.
if [ ! -d "$ROLE" ]; then
    mkdir "$ROLE"
fi

# Add missing role directories
ROLE_DIRS=("$ROLE/tasks")
for DIR in "${ROLE_DIRS[@]}"
do
    if [ ! -d "$ROLE/$DIR" ]; then
        mkdir -p "$DIR"
    fi
done

# Create .ansible-lint file if it doesn't exist.
if [ ! -f "$ROLE/.ansible-lint" ]; then
    cat <<EOF > "$ROLE/.ansible-lint"
---
skip_list:
  - no-handler
  - line-length
EOF
fi

# Create .yamllint file if it doesn't exist.
if [ ! -f "$ROLE/.yamllint" ]; then
    cat <<EOF > "$ROLE/.yamllint"
---
extends: default

rules:
  line-length: disable
EOF
fi

# Create main.yml task for role if it doesn't exist.
if [ ! -f "$ROLE/tasks/main.yml" ]; then
    cat <<EOF > "$ROLE/tasks/main.yml"
---
EOF
fi

# Initialize git repository for role
if [ ! -d "$ROLE/.git" ]; then
    if which git &> /dev/null; then
      pushd "$ROLE" > /dev/null
      git init --quiet --initial-branch=main
      popd > /dev/null
    else
        echo "ERROR: git command not found."
        return -1
    fi
fi

# Add git hook for secrets
if [ ! -f "$ROLE/.git/hooks/pre-commit" ]; then
    pushd "$ROLE/.git/hooks" > /dev/null
    curl -s -o pre-commit https://gitlab.com/cubt85iz/bash-utilities/-/raw/main/pre-commit.hook.sh
    popd > /dev/null
fi
chmod +x "$ROLE/.git/hooks/pre-commit"
