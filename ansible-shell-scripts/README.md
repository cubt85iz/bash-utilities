# ansible-shell-scripts

## Installation

```sh
curl -s https://gitlab.com/cubt85iz/bash-utilities/-/raw/main/ansible-shell-scripts/install-ansible-utilities.sh | bash
```

## Usage

### ansible-clone
Clones an ansible repository (including submodules) and applies [pre-commit hook](https://gitlab.com/cubt85iz/bash-utilities/-/blob/main/pre-commit.hook.sh) to prevent leakage of secrets.
```sh
ansible-clone <repository_url>
```

### ansible-init-playbook
Creates a new playbook including lint files (.yamllint & .ansible-lint), initializes a git repository, and applies [pre-commit hook](https://gitlab.com/cubt85iz/bash-utilities/-/blob/main/pre-commit.hook.sh) to prevent leakage of secrets.
```sh
ansible-init-playbook <playbook_path>
```

### ansible-init-role
Creates a new role including lint files (.yamllint & .ansible-lint), initializes a git repository, and applies [pre-commit hook](https://gitlab.com/cubt85iz/bash-utilities/-/blob/main/pre-commit.hook.sh) to prevent leakage of secrets.
```sh
ansible-init-role <role_path>
```