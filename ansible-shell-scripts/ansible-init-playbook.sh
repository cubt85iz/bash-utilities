#!/usr/bin/env bash

display_usage() {
    echo "ERROR: Playbook path missing."
}

PLAYBOOK="$1"

# Create playbook directory if it doesn't exist.
if [ ! -d "$PLAYBOOK" ]; then
    mkdir "$PLAYBOOK"
fi

# Add missing playbook directories
PLAYBOOK_DIRS=("host_vars" "group_vars" "roles" "inventory")
for DIR in "${PLAYBOOK_DIRS[@]}"
do
    if [ ! -d "$PLAYBOOK/$DIR" ]; then
        mkdir -p "$PLAYBOOK/$DIR"
    fi
done

# Create .ansible-lint file if it doesn't exist.
if [ ! -f "$PLAYBOOK/.ansible-lint" ]; then
    cat <<EOF > "$PLAYBOOK/.ansible-lint"
---
skip_list:
  - no-handler
  - line-length
EOF
fi

# Create .yamllint file if it doesn't exist.
if [ ! -f "$PLAYBOOK/.yamllint" ]; then
    cat <<EOF > "$PLAYBOOK/.yamllint"
---
extends: default

rules:
  line-length: disable
EOF
fi

# Create yml files for playbook if they don't exist.
PLAYBOOK_FILES=("local.yml" "requirements.yml")
for FILE in "${PLAYBOOK_FILES[@]}"
do
    if [ ! -f "$PLAYBOOK/$FILE" ]; then
        cat <<EOF > "$PLAYBOOK/$FILE"
---
EOF
    fi
done

# Add missing playbook files
PLAYBOOK_FILES=("ansible.cfg" "inventory/hosts")
for FILE in "${PLAYBOOK_FILES[@]}"
do
    if [ ! -f "$PLAYBOOK/$FILE" ]; then
        if [ -f "$FILE" ]; then
            cp "$FILE" "$PLAYBOOK/$FILE"
        else
            touch "$PLAYBOOK/$FILE"
        fi
    fi
done

# Initialize git repository for playbook
if [ ! -d "$PLAYBOOK/.git" ]; then
    if which git &> /dev/null; then
      pushd "$PLAYBOOK" > /dev/null
      git init --quiet --initial-branch=main
      popd > /dev/null
    else
        echo "ERROR: git command not found."
        return -1
    fi
fi

# Add git hook for secrets
if [ ! -f "$PLAYBOOK/.git/hooks/pre-commit" ]; then
    pushd "$PLAYBOOK/.git/hooks" > /dev/null
    curl -s -o pre-commit https://gitlab.com/cubt85iz/bash-utilities/-/raw/main/pre-commit.hook.sh
    popd > /dev/null
fi
chmod +x "$PLAYBOOK/.git/hooks/pre-commit"
