#!/usr/bin/env bash

# Download and configure ansible-clone utility.
if [ ! -f ~/.local/bin/ansible-clone ]; then
    curl -s -o ~/.local/bin/ansible-clone https://gitlab.com/cubt85iz/bash-utilities/-/raw/main/ansible-shell-scripts/ansible-clone.sh
    chmod +x ~/.local/bin/ansible-clone
fi

# Download and configure ansible-init-playbook utility.
if [ ! -f ~/.local/bin/ansible-init-playbook ]; then
    curl -s -o ~/.local/bin/ansible-init-playbook https://gitlab.com/cubt85iz/bash-utilities/-/raw/main/ansible-shell-scripts/ansible-init-playbook.sh
    chmod +x ~/.local/bin/ansible-init-playbook
fi

# Download and configure ansible-init-role utility.
if [ ! -f ~/.local/bin/ansible-init-role ]; then
    curl -s -o ~/.local/bin/ansible-init-role https://gitlab.com/cubt85iz/bash-utilities/-/raw/main/ansible-shell-scripts/ansible-init-role.sh
    chmod +x ~/.local/bin/ansible-init-role
fi