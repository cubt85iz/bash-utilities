#!/usr/bin/env bash

REPO="$1"
REPO_NAME="${REPO##*\/}"
REPO_FOLDER="${REPO_NAME%.*}"

echo "REPO: $REPO"
echo "NAME: $REPO_NAME"
echo "FOLDER: $REPO_FOLDER"

# Clone Repo
if [ ! -d "$REPO_FOLDER" ]; then
    git clone --recurse-submodules "$REPO"

    # Add pre-commit hook
    pushd "$REPO_FOLDER/.git/hooks" > /dev/null
    curl -s -o pre-commit https://gitlab.com/cubt85iz/bash-utilities/-/raw/main/pre-commit.hook.sh
    chmod +x pre-commit
    popd > /dev/null
fi