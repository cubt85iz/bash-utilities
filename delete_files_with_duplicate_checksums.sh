#!/usr/bin/env bash

# Description: Finds files with duplicate hashes in a checksum file.
# Prerequisites: Use the command `find . -type f -exec md5sum "{}" + > checksums.list`
#  to create a list of checksums for all files in the current directories files and
#  subdirectories. Use `sort -o <output_file> checksums.list` to sort the checksums and
#  group duplicates.
# Arguments: Path to the generate checksum file.

MD5_FILE="$1"
PREV_MD5=
DUP_FILES=()
while read line; do
  PATTERN='([0-9a-f]{32})\s+(.+)'
  if [[ $line =~ $PATTERN ]]; then
    CUR_MD5="${BASH_REMATCH[1]}"
    CUR_FILE="${BASH_REMATCH[2]}"

    if [ "$CUR_MD5" == "$PREV_MD5" ]; then
      DUP_FILES+=("$CUR_FILE")
    else
      if [ ${#DUP_FILES[@]} -gt 1 ]; then
        echo "Hash: $PREV_MD5"
        echo "Files: "
        index=0
        for dup in "${DUP_FILES[@]}"; do
          echo "[$index]  $dup"
          ((index=$index+1))
        done

        DEL_ALL=0
        DEL_NONE=0
        KEEP_NUM=
        read -p "Keep Files [#|(n)one|A(ll)]? " choice </dev/tty
        case "$choice" in
          n|N ) DEL_ALL=1;;
          a|A ) DEL_NONE=1;;
          * ) KEEP_NUM="$choice"
        esac

        if [ "$DEL_NONE" != "1" ]; then
          if [ "$DEL_ALL" == "1" ]; then
            echo "Deleting all files with hash: $PREV_MD5"
            for dup in "${DUP_FILES[@]}"; do
              rm "$dup"
            done
          else
            if [[ ! -z "$KEEP_NUM" && "$KEEP_NUM" =~ [0-9]+ ]]; then
              # No bounds checking here yet. Provide valid inputs!
              KEEP_FILE="${DUP_FILES[$KEEP_NUM]}"
              for dup in "${DUP_FILES[@]}"; do
                if [ "$dup" != "$KEEP_FILE" ]; then
                  rm "$dup"
                fi
              done
            fi
          fi
        fi

        # Echo new line to separate multiple duplicates.
        echo
      fi

      DUP_FILES=("$CUR_FILE")
      PREV_MD5="$CUR_MD5"
    fi
  fi
done <$MD5_FILE

