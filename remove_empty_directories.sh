#!/usr/bin/env bash

DIR=$1

if [ !z "$DIR" ]; then
    find "$DIR" -type d -empty -exec rmdir {} \;
else
    echo "ERROR: Specify directory to search for empty folders."
    exit 1
fi
