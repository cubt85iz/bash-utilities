#!/usr/bin/env bash

chown -R philip:972 /tank/philip/Media/Video/Movies/Features
find /tank/philip/Media/Video/Movies/Features -type d -exec chmod 775 {} \;
find /tank/philip/Media/Video/Movies/Features -type f -exec chmod 664 {} \;
