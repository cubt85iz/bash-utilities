#!/usr/bin/env bash

export CIU_DATE=$(date +%Y%m%d)
export CIU_TIME=$(date +%H:%M:%S)
export CIU_SETTINGS_FILE="~/.ciu/settings.json"
export CIU_LOG_FILE="/var/log/ciu/container_images_updater.$CIU_DATE.log"
export CIU_CONTAINER_IMAGES=$(docker ps --format '{{ .Image }}' | sort -u)
export CIU_IMAGE_ALLOWLIST=
export CIU_IMAGE_BLOCKLIST=

info() { printf "%s %s\n" "$(date)" "$*" 2>>$CIU_LOG_FILE 1>&2; }
warn() { info "WARN: $*"; }
error() { info "ERROR: $*"; return -1; }
trap 'echo $(date) Container Image Update Interrupted >&2; exit 2' INT TERM

initialize_log_file() {
    CIU_LOG_DIR=$(dirname "$CIU_LOG_FILE")
    if [ ! -d "$CIU_LOG_DIR" ]; then
        mkdir -p "$CIU_LOG_DIR"
    fi

    if [ ! -f "$CIU_LOG_FILE" ]; then
        touch "$CIU_LOG_FILE"
    fi

    # Write session header to the log file.
    printf "\n\n===== %s %s =====\n" "$CIU_DATE" "$CIU_TIME" >> "$CIU_LOG_FILE"
}

# Gets the images that must be updated. Adding an image to this list will
# cause it to always be updated. List base images for docker builds here.
get_image_allow_list() {
    if [ -f "$CIU_SETTINGS_FILE" ]; then
        CIU_IMAGE_ALLOWLIST=$(jq -r '.[].allow_list[]' "$CIU_SETTINGS_FILE")
        info "Allow list: $(echo ${CIU_IMAGE_ALLOWLIST[*]})"
    fi
}

# Gets the images that are prohibited from being updated.
get_image_block_list() {
    if [ -f "$CIU_SETTINGS_FILE" ]; then
        CIU_IMAGE_BLOCKLIST=$(jq -r '.[].block_list[]' "$CIU_SETTINGS_FILE")
        info "Block list: $(echo ${CIU_IMAGE_BLOCKLIST[*]})"
    fi
}

pull_container_image() {
    CONTAINER_IMAGE=$1
    if [ ! -z "$CONTAINER_IMAGE" ]; then
        OLD_CONTAINER_IMAGE=${CONTAINER_IMAGE%:*}:$CIU_DATE
        if docker tag "$CONTAINER_IMAGE" "$OLD_CONTAINER_IMAGE" 2>> $CIU_LOG_FILE 1>&2; then
            info "Successfully created the intermediate tag: '$OLD_CONTAINER_IMAGE'."
            if docker image rm "$CONTAINER_IMAGE" 2>> $CIU_LOG_FILE 1>&2; then
                info "Successfully removed the image '$CONTAINER_IMAGE'."
                if docker pull --quiet "$CONTAINER_IMAGE" 2>> $CIU_LOG_FILE 1>&2; then
                    info "Successfully pulled the latest container image '$CONTAINER_IMAGE'."

                    NEW_IMAGE_ID=$(docker images --format '{{ .ID }}' $CONTAINER_IMAGE)
                    OLD_IMAGE_ID=$(docker images --format '{{ .ID }}' $OLD_CONTAINER_IMAGE)
                    if [ "$NEW_IMAGE_ID" == "$OLD_IMAGE_ID" ]; then
                        info "Current image for $CONTAINER_IMAGE is the latest version."
                        docker image rm "${CONTAINER_IMAGE%:*}:$CIU_DATE" 2>> $CIU_LOG_FILE 1>&2
                    else
                        warn "Current image for $CONTAINER_IMAGE is out of date. A new image has been downloaded and will be applied in the near future."
                    fi
                else
                    error "Failed to download the latest image for $CONTAINER_IMAGE."
                fi
            else
                error "Failed to remove the latest image for $CONTAINER_IMAGE."
            fi
        else
            error "Failed to create a new tag for the image $CONTAINER_IMAGE."
        fi
    else
        error "Failed to provide a valid container image."
    fi
}

# Initialize log file for session.
initialize_log_file

# Output detected images to the log file.
info "Detected Images: $(echo ${CIU_CONTAINER_IMAGES[*]})"

# Get images in the block and allow lists.
get_image_allow_list
get_image_block_list

# Append images from the allow list to the detected images.
CIU_CONTAINER_IMAGES+=( "${CIU_IMAGE_ALLOWLIST[@]}" )

# Prune images from block list.
CIU_CONTAINER_IMAGES_TO_PROCESS=()
for IMAGE in ${CIU_CONTAINER_IMAGES[@]}
do
    if [[ ! "${CIU_IMAGE_BLOCKLIST[*]}" =~ "${IMAGE}" ]]; then
        CIU_CONTAINER_IMAGES_TO_PROCESS+=( "${IMAGE}" )
    fi
done
info "Images to Pull: $(echo ${CIU_CONTAINER_IMAGES_TO_PROCESS[*]})"

# Iterate over all images and pull latest container images.
for IMAGE in ${CIU_CONTAINER_IMAGES_TO_PROCESS[@]}
do
    # If there's a new image this command will fail to remove the
    # image as it's in use by the current container.
    pull_container_image $IMAGE
done
