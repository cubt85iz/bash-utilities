#!/usr/bin/env bash

display_usage() {
        echo "ERROR: Insufficient arguments. Valid Syntax: move_movies_to_subdirectories MOVIE-DIR"
        exit 1
}

# Set BASEDIR to first provided argument
if [ ! -z "$1" ]; then
        BASEDIR="$1"
else
        display_usage
fi

# Iterate over files
while read movie; do
        FILENAME=$(basename "$movie")
        MOVIENAME="${FILENAME%.*}"

        echo
        echo "=============================="
        echo "Movie: $MOVIENAME"
        echo "=============================="


        if [ ! -d "$BASEDIR/$MOVIENAME" ]; then
                if mkdir "$BASEDIR/$MOVIENAME"; then
                        echo "Created $BASEDIR/$MOVIENAME."
                else
                        echo "ERROR: Failed to create $BASEDIR/$MOVIENAME."
                        exit 1
                fi
        fi

        if [ ! -f "$BASEDIR/$MOVIENAME/$FILENAME" ]; then
                if mv "$BASEDIR/$FILENAME" "$BASEDIR/$MOVIENAME/$FILENAME"; then
                        echo "Moved $FILENAME to $BASEDIR/$MOVIENAME/."
                else
                        echo "Failed to move $FILENAME to $BASEDIR/$MOVIENAME/."
                        exit 1
                fi
        fi
done <<< "$(find $BASEDIR -maxdepth 1 -type f \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.m4v' \) | sort)"
