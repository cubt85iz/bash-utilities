#!/usr/bin/env bash

LANGUAGE="$1"

if [ -z "$LANGUAGE" ]; then
  echo "Provide three letter language code."
  return 1
fi

for FILE in ./*.mp4
do
  CONVERTED_FILE=${FILE:0:-4}.$LANGUAGE.mp4
  echo $FILE
  echo $CONVERTED_FILE
  ffmpeg -i "$FILE" -c copy -metadata:s:a:0 language=$LANGUAGE "$CONVERTED_FILE"
done
