#!/usr/bin/env bash

# Lint *.yml files using yamllint
run_yaml_linter() {
  if which yamllint &> /dev/null; then
    yamllint .
  fi
}

# Line *.yml files using ansible-lint
run_ansible_linter() {
  if which ansible-lint &> /dev/null; then
    ansible-lint .
  fi
}

# Run all linters
run_linters() {
  RESULT=$(run_yaml_linter && run_ansible_linter)
  return $RESULT
}

# Find any unencrypted files in directories that typically contain secrets.
find_unencrypted_secrets() {
  UNENCRYPTED_FILES=()
  SECRET_FOLDERS=$(find -type d \( -iname "host_vars" -o -iname "group_vars" \))
  echo $SECRET_FOLDERS
  for FOLDER in $SECRET_FOLDERS
  do
    UNENCRYPTED_FILES+=($(find $FOLDER -type f -iname "*.yml" -exec grep -riL "\$ANSIBLE_VAULT;1.1;AES256" "{}" \;))
  done

  if [ ${#UNENCRYPTED_FILES[@]} -ne 0 ]; then
    echo "ERROR: Found the following unencrypted files in directories that typically contain secrets:"
    for FILE in ${UNENCRYPTED_FILES[@]}
    do
      echo "  - $FILE"
    done
  fi

  return ${#UNENCRYPTED_FILES[@]}
}

find_unstaged_secrets() {
  UNSTAGED_SECRETS=()
  UNSTAGED_FILES=$(git diff --name-only)
  for FILE in $UNSTAGED_FILES
  do
    if [[ $FILE == host_vars* ]] || [[ $FILE == group_vars* ]]; then
      UNSTAGED_SECRETS+=($FILE)
    fi
  done

  if [ ${#UNSTAGED_SECRETS[@]} -ne 0 ]; then
    echo "ERROR: Found the following unstaged files in directories that typically contain secrets:"
    for FILE in ${UNSTAGED_SECRETS[@]}
    do
      echo "  - $FILE"
    done
  fi

  return ${#UNSTAGED_SECRETS[@]}
}

if run_linters; then
  if find_unstaged_secrets; then
    find_unencrypted_secrets
  fi
fi